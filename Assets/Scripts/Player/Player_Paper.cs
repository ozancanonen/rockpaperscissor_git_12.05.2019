﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Player_Paper : MonoBehaviour
{
    private Rigidbody2D rb;
    private bool hordeIsGrounded;//the bool that indicates if the player is touching the ground
    public Transform feetPos;//the position where the plater's feet are for checking if is touching ground
    //public float checkRadius; we used the check the ground by a circle if we want to change here it is
    public Vector2 checkBoxRadius;//the x and y size of the box for checking ground
    public float jumpForce;//the amount of height the playe ris going to jump 
    public LayerMask whatIsGround;//choosing on inspector the objects with tag "ground"
    public float jumpTime;//the amount of time the player is jumping
    private GameManager gm;
    private float startTime;//to make the lerps(where the players go forawrd when they are too behind) more smooth we take the time
    private float startTimeForMix;//to make the lerps(mixing the players) more smooth we take the time
    private float journeyLength;//the distance the player's are going to go forward
    bool postControllerCheck;//the bool which calls the forward command if collided with posController
    bool kingPostControllerCheck;//the bool which calls the forward command for kinf if collided with posController(there are two different methods because king must be always the most forward one)
    private float speed = 0.1f;//going forward speed when the player's are behind
    private float jumpDelayForHorde;//jump delay for the horde
    private bool dontJump;//bool which controls the hord's jump
    private int playersNumberInHorde;
    private int playerJumpNumber;
    private Vector3 thisPlayerPos;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        gm = GetComponentInParent<GameManager>();
        gm.jumpTimeCounterHorde = jumpTime;
        playersNumberInHorde = gm.allyScore;
        jumpDelayForHorde = gm.jumpDelay;
        thisPlayerPos = gameObject.transform.position;
        thisPlayerPos.y = -2.71f;
        playerJumpNumber = gm.kingPlayerJumpNumber;
        if (gameObject.GetComponent<SpriteRenderer>() != null)
        {
            gameObject.GetComponent<SpriteRenderer>().sortingOrder = 10 - playersNumberInHorde;
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.transform.tag == "Enemy")
        {
            gm.decreaseAllyScore();
            if (gm.allyScore == 0)
            {
                gm.HordeList.Remove(gameObject);
                Destroy(gameObject);
                SceneManager.LoadScene("Lose Screen");
            }
            else
            {
                if (gameObject.name == "Player_Paper")
                {
                    gm.creatingNewPaperKingPlayer();
                }
                Destroy(gameObject);
                Destroy(col.gameObject); 
            }
        }

        if (col.transform.tag == "CounterRace")
        {
            gm.counterRaceScoreCollider++;
            if (gm.counterRaceScoreCollider >= 3)
            {
                gm.counterRaceScoreCollider = 0;
                Destroy(col.gameObject);
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.transform.tag == "Coin")
        {
            gm.addCoinScore();
            Destroy(col.gameObject);
        }
        if (col.transform.tag == "ScoreZone")
        {
            gm.counterRaceScoreCollider = 0;
            kingPostControllerCheck = false;
            postControllerCheck = false;
            gm.addScore();
        }
        if (gameObject.name != "Player_Paper" && col.gameObject.tag == "PosController" && gameObject.transform.position.x< thisPlayerPos.x)
        {
                postControllerCheck = true;
                startTime = Time.time;
                journeyLength = Vector3.Distance(transform.position, thisPlayerPos);
        }
        if (gameObject.name == "Player_Paper" && col.gameObject.tag == "PosController" &&  gameObject.transform.position.x < gm.kingPlayerPos.x)
        {
            kingPostControllerCheck = true;
            startTime = Time.time;
            journeyLength = Vector3.Distance(transform.position, gm.kingPlayerPos);
        }
    }
    void FixedUpdate()
    {
        if (rb.velocity.y > 0)
        {
            gameObject.GetComponent<Animator>().SetBool("isJumping", true);
        }
        else
        {
            gameObject.GetComponent<Animator>().SetBool("isJumping", false);
        }

        if (rb.velocity.y < 0)
        {
            gameObject.GetComponent<Animator>().SetBool("isFalling", true);
        }
        else
        {
            gameObject.GetComponent<Animator>().SetBool("isFalling", false);
        }
    
        if (kingPostControllerCheck)
        {
            float distCovered = (Time.time - startTime) * 1f;
            float fracJourney = distCovered / journeyLength;
            gameObject.transform.position = Vector3.Lerp(transform.position, gm.kingPlayerPos, fracJourney * speed);
            if (gameObject.transform.position.x >= gm.kingPlayerPos.x)
            {
                kingPostControllerCheck = false;
            }
        }

        if (postControllerCheck)
        {
            float distCovered = (Time.time - startTime) * 1f;
            float fracJourney = distCovered / journeyLength;
            gameObject.transform.position = Vector3.Lerp(transform.position, thisPlayerPos, fracJourney * speed);
            if (gameObject.transform.position.x >= thisPlayerPos.x)
            {
                kingPostControllerCheck = false;
            }
        }
    }

        void Update()
        {
        if (gameObject.name == "Player_Paper")//king player jump movement
        {
            gm.isGrounded = Physics2D.OverlapBox(feetPos.position, checkBoxRadius, 0f, whatIsGround);
            if (gm.isGrounded)
            {
                gameObject.GetComponent<Animator>().SetBool("isJumping", false);
                gameObject.GetComponent<Animator>().SetBool("isFalling", false);
                gm.playerIsJumping = false;
                rb.gravityScale = 3;//kağıt ve rock da gerekiyor
            }
            if (gm.isGrounded && Input.GetMouseButton(0) && gm.buttonUp)
            {
                gm.kingPlayerJumpNumber++;
                gm.buttonUp = false;
                gm.isJumping = true;
                gm.playerIsJumping = true;
                gm.jumpTimeCounter = jumpTime;
                gm.jumpTimeCounterHorde = jumpTime;
                rb.velocity = Vector2.up * jumpForce;
            }
            if (Input.GetMouseButton(0) && gm.isJumping && !gm.buttonUp)
            {
                if (gm.jumpTimeCounter >= 0)
                {
                    gm.playerIsJumping = true;
                    rb.velocity = Vector2.up * jumpForce;
                    gm.countTime();
                }
                else
                {
                    if (rb.velocity.y <= 10)
                    {
                        rb.gravityScale = 1;//kağıt için gerekiyor
                    }
                    if (gm.jumpTimeCounterHorde > 0)
                    {
                        gm.jumpTimeCounterHorde = 0;
                    }
                    gm.isJumping = false;
                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                gm.isJumping = false;
                gm.buttonUp = true;
                rb.gravityScale = 3;//kağıt için gerekiyor
                //rb.gravityScale = 6;//rock için gerekiyor
                if (gm.jumpTimeCounterHorde > 0)
                {
                    gm.jumpTimeCounterHorde = 0;
                }
            }
        }


        else//horde jump movement
        {
            hordeIsGrounded = Physics2D.OverlapBox(feetPos.position, checkBoxRadius, 0f, whatIsGround);
            if (hordeIsGrounded)
            {
                rb.gravityScale = 3;//kağıt ve rock için gerekiyor
                dontJump = true;
            }
            if (/*gm.kingPlayerJumpNumber>playerJumpNumber&&*/ gm.isJumping && hordeIsGrounded && gm.playerIsJumping && gm.jumpTimeCounterHorde < jumpTime - (jumpDelayForHorde * 2))
            {
                playerJumpNumber = gm.kingPlayerJumpNumber;
                rb.velocity = Vector2.up * jumpForce;
                dontJump = false;

            }
            if (!dontJump && !gm.isGrounded && gm.playerIsJumping && !hordeIsGrounded)
            {
                if (gm.jumpTimeCounterHorde > -jumpDelayForHorde *gm.allyScore && gm.jumpTimeCounterHorde < jumpTime - (jumpDelayForHorde * 2) && gm.playerIsJumping)
                {
                    rb.velocity = Vector2.up * jumpForce;
                    if (!gm.isJumping)
                    {
                        gm.countTimeHorde();
                    }
                }
                else
                {
                    //rb.gravityScale = 6; //rock için gerekiyor
                    if (Input.GetMouseButton(0))//kağıt için gerekiyor
                    {
                        rb.gravityScale = 1f;
                    }
                    dontJump = true;
                }
            }
            if (Input.GetMouseButtonUp(0))//kağıt için gerekiyor
            {
                rb.gravityScale = 3;
            }
        }
    }   
}
    

