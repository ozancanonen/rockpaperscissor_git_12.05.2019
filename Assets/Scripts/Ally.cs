﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ally : MonoBehaviour
{
    private GameManager gm;
    public GameObject Player;
    int number;
    private float spawnX = 0.4f;
    private float jDelay = 0.02f;

    void Start()
    {
        gm = GetComponentInParent<GameManager>();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.transform.tag == "Player")
        {
            Destroy(gameObject);
            gm.allySpawnPosX = spawnX * gm.allyScore;
            gm.spawnZForHorde--;
            var newPlayer = Instantiate(Player, col.gameObject.transform.position + new Vector3(-gm.allySpawnPosX, +1f, gm.spawnZForHorde), Quaternion.identity);
            gm.jumpDelay = jDelay * gm.allyScore;
            number = Random.Range(0, 1);
            //if (number == 0)
            //{
            //    newPlayer.GetComponent<CapsuleCollider2D>().offset = new Vector2(0f, -0.1f);
            //}
            gm.HordeList.Add(newPlayer);
            //newPlayer.GetComponent<SpriteRenderer>().sortingOrder = 20;//rock sprite renderer yok eror veriyor
            newPlayer.transform.SetParent(gm.transform.GetChild(1));
            gm.addAllyScore();
            
        }
    }

}
