﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneLoader : MonoBehaviour
{
    public void LoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex + 1);
    }

    public void LoadStartScene()
    {
      
        SceneManager.LoadScene(0);
    }
    public void LoadRockScene()
    {

        SceneManager.LoadScene("Rock");
    }
    public void LoadPaperScene()
    {

        SceneManager.LoadScene("Paper");
    }
    public void LoadScissorScene()
    {

        SceneManager.LoadScene("Scissor");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
