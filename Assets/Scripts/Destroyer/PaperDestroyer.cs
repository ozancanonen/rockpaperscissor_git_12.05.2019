﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PaperDestroyer : MonoBehaviour
{
    private GameManager gm;

    void Start()
    {
        gm = GetComponentInParent<GameManager>();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (gm.allyScore == 1)
            {
                Destroy(col.gameObject);
                SceneManager.LoadScene("Lose Screen");
            }
            else
            {
                if (col.gameObject.name == "Player_Paper")
                {
                    gm.creatingNewPaperKingPlayer();
                }
                gm.decreaseAllyScore();
                gm.HordeList.Remove(col.gameObject);
                Destroy(col.gameObject);

            }

        }
        else
        {
            Destroy(col.gameObject);
        }
    }
}
