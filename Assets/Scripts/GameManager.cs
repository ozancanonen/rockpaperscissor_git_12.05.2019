﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    public TextMeshProUGUI allyScoreText;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI coinScoreText;
    public TextMeshProUGUI highscoreText;
    public GameObject crown;
    private int coinScore;
    [HideInInspector] public int score;
    [HideInInspector] public float jumpDelay;
    GameObject kingPlayer;//the player which jumps first and the one who is most forward
    [HideInInspector] public float allySpawnPosX;//the x value of ally's spawn position
    [HideInInspector] public List<GameObject> HordeList;//the players list
    [HideInInspector] public bool playerIsJumping;
    [HideInInspector] public int allyScore;
    [HideInInspector] public int spawnZForHorde;
    [HideInInspector] public int counterRaceScoreCollider;//the number of players are colliding with the counter race
    [HideInInspector] public float jumpTimeCounter;
    [HideInInspector] public int kingPlayerJumpNumber;
    [HideInInspector] public float jumpTimeCounterHorde;
    [HideInInspector] public bool isJumping;
    [HideInInspector] public bool isGrounded;//the bool that indicates if the player is touching the ground
    [HideInInspector] public bool buttonUp = true;//the bool who checks if the player released the jump or not
    public Vector3 kingPlayerPos = new Vector3(-3.85f, -2.7f, 0);

    private void Start()
    {
        HordeList = new List<GameObject>(GameObject.FindGameObjectsWithTag("Player"));
        highscoreText.text ="Highscore:"+ PlayerPrefs.GetInt("HighScore");  
    }

    public void setHighscore()
    {
        if (score > PlayerPrefs.GetInt("HighScore"))
        {
            PlayerPrefs.SetInt("HighScore", score);
            highscoreText.text = "Highscore:" + score;
        }

    }
    public void addAllyScore()
    {
        allyScore++;
        allyScoreText.text = "Ally:" + allyScore;
    }
    public void decreaseAllyScore()
    {
        allyScore--;
        allyScoreText.text = "Ally:" + allyScore;
    }
    public void addScore()
    {
        score++;
        scoreText.text = "Score:" + score;
        setHighscore();
    }
    public void addCoinScore()
    {
        coinScore++;
        coinScoreText.text = ":" + coinScore;
    }
    public void countTime()
    {
        jumpTimeCounter -= Time.deltaTime;
        countTimeHorde();
    }
    public void countTimeHorde()
    {
        jumpTimeCounterHorde -= Time.deltaTime;
    }

    public void creatingNewPaperKingPlayer()
    {
        float bestX=-8;
        
        HordeList= new List<GameObject>(GameObject.FindGameObjectsWithTag("Player"));
        foreach (GameObject playerFromHorde in HordeList)
        {
            if (playerFromHorde.name!= "Player_Paper")
            {
                if (playerFromHorde.transform.position.x > bestX)
                {
                    bestX = playerFromHorde.transform.position.x;
                    kingPlayer = playerFromHorde;
                }
            }
        }
        if (kingPlayer != null)
        {
            kingPlayer.name = "Player_Paper";
            crown.transform.SetParent(kingPlayer.transform);
            crown.transform.localPosition = new Vector3(-0.1f, 1.1f, 0);
        }
    }
    public void creatingNewRockKingPlayer()
    {
        float bestX = -8;

        HordeList = new List<GameObject>(GameObject.FindGameObjectsWithTag("Player"));
        foreach (GameObject playerFromHorde in HordeList)
        {
            if (playerFromHorde.name != "Player_Rock")
            {
                if (playerFromHorde.transform.position.x > bestX)
                {
                    bestX = playerFromHorde.transform.position.x;
                    kingPlayer = playerFromHorde;
                }
            }
        }
        if (kingPlayer != null)
        {
            kingPlayer.name = "Player_Rock";
            crown.transform.SetParent(kingPlayer.transform);
            crown.transform.localPosition = new Vector3(-0.1f, 6.3f, 0);
        }
    }
    public void creatingNewScissorKingPlayer()
    {
        float bestX = -8;

        HordeList = new List<GameObject>(GameObject.FindGameObjectsWithTag("Player"));
        foreach (GameObject playerFromHorde in HordeList)
        {
            if (playerFromHorde.name != "Player_Scissor")
            {
                if (playerFromHorde.transform.position.x > bestX)
                {
                    bestX = playerFromHorde.transform.position.x;
                    kingPlayer = playerFromHorde;
                }
            }
        }
        if (kingPlayer != null)
        {
            kingPlayer.name = "Player_Scissor";
            crown.transform.SetParent(kingPlayer.transform);
            crown.transform.localPosition = new Vector3(-0.1f, 1.1f, 0);
        }
    }
}
    

